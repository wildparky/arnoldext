import os

HOME = os.getenv("HOME")

ARNOLD_VERSION = "4.2.0.6"
ARNOLD_ROOT = "%s/vfx/arnold/%s" % (HOME, ARNOLD_VERSION)

VERSION = "0.1.0"
INSTALL_ROOT = "%s/vfx/fabric/Exts/Arnold%s" % (HOME, VERSION)