#include "Arnold.h"
#include "Arnold_types.h"

#include <ai.h>

FABRIC_EXT_EXPORT void Arnold_NodeEntryLookUp(AtNodeEntryResult result, StringIN name)
{
	result.ptr = (AtNodeEntry*)AiNodeEntryLookUp(name.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetName(StringResult result, AtNodeEntryIN nentry)
{
	result = AiNodeEntryGetName((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::SInt32 Arnold_NodeEntryGetType(AtNodeEntryIN nentry)
{
	return AiNodeEntryGetType((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetTypeName(StringResult result, AtNodeEntryIN nentry)
{
	result = AiNodeEntryGetTypeName((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::SInt32 Arnold_NodeEntryGetOutputType(AtNodeEntryIN nentry)
{
	return AiNodeEntryGetOutputType((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetFilename(StringResult result, AtNodeEntryIN nentry)
{
	result = AiNodeEntryGetFilename((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetVersion(StringResult result, AtNodeEntryIN nentry)
{
	result = AiNodeEntryGetVersion((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::SInt32 Arnold_NodeEntryGetCount(AtNodeEntryIN nentry)
{
	return AiNodeEntryGetCount((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::SInt32 Arnold_NodeEntryGetNumParams(AtNodeEntryIN nentry)
{
	return AiNodeEntryGetNumParams((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetParameter(AtParamEntryResult result, AtNodeEntryIN nentry, SInt32IN i)
{
	result.ptr = (AtParamEntry*)AiNodeEntryGetParameter((AtNodeEntry*)nentry.ptr, i);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryLookUpParameter(AtParamEntryResult result, AtNodeEntryIN nentry, StringIN name)
{
	result.ptr = (AtParamEntry*)AiNodeEntryLookUpParameter((AtNodeEntry*)nentry.ptr, name.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetParamIterator(AtParamIteratorResult result, AtNodeEntryIN nentry)
{
	result.ptr = (AtParamIterator*)AiNodeEntryGetParamIterator((AtNodeEntry*)nentry.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryGetMetaDataIterator(AtMetaDataIteratorResult result, AtNodeEntryIN nentry, 
															StringIN param)
{
	const char* paramc = NULL;
	if (!strlen(param.data())) paramc = param.data();
	result.ptr = (AtMetaDataIterator*)AiNodeEntryGetMetaDataIterator((AtNodeEntry*)nentry.ptr, paramc);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryInstall(SInt32IN type, ByteIN output_type, StringIN name, StringIN filename, 
												AtNodeMethodsIN methods, StringIN version)
{
	AiNodeEntryInstall(type, output_type, name.data(), filename.data(), (AtNodeMethods*)methods.ptr, version.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryUninstall(StringIN name)
{
	AiNodeEntryUninstall(name.data());
}

FABRIC_EXT_EXPORT void Arnold_ParamIteratorDestroy(AtParamIteratorIO iter)
{
   AiParamIteratorDestroy((AtParamIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_ParamIteratorGetNext(AtParamEntryResult result, AtParamIteratorIO iter)
{
   result.ptr = 
      (AtParamEntry*)AiParamIteratorGetNext((AtParamIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_ParamIteratorFinished(AtParamIteratorIN iter)
{
   return AiParamIteratorFinished((AtParamIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_MetaDataIteratorDestroy(AtMetaDataIteratorIO iter)
{
   AiMetaDataIteratorDestroy((AtMetaDataIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_MetaDataIteratorGetNext(AtMetaDataEntryResult result, AtMetaDataIteratorIO iter)
{
   result.ptr = 
      (AtMetaDataEntry*)AiMetaDataIteratorGetNext((AtMetaDataIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_MetaDataIteratorFinished(AtMetaDataIteratorIN iter)
{
   return AiMetaDataIteratorFinished((AtMetaDataIterator*)iter.ptr);
}

