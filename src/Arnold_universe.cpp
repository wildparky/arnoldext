#include "Arnold.h"
#include "Arnold_types.h"
#include <ai.h>

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_UniverseIsActive()
{
   return AiUniverseIsActive();
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_UniverseCacheFlush(SInt32IN flags)
{
   return AiUniverseCacheFlush(flags);
}

FABRIC_EXT_EXPORT void Arnold_UniverseGetOptions(AtNodeResult result)
{
   result.ptr = (AtNode*)AiUniverseGetOptions();
}

FABRIC_EXT_EXPORT void Arnold_UniverseGetCamera(AtNodeResult result)
{
   result.ptr = (AtNode*)AiUniverseGetCamera();
}

FABRIC_EXT_EXPORT void Arnold_UniverseGetSceneBounds(Box3Result result)
{
   AtBBox bbox = AiUniverseGetSceneBounds();
   result.min.x = bbox.min.x;
   result.min.y = bbox.min.y;
   result.min.z = bbox.min.z;
   result.max.x = bbox.max.x;
   result.max.y = bbox.max.y;
   result.max.z = bbox.max.z;
}

FABRIC_EXT_EXPORT void Arnold_UniverseGetNodeIterator(AtNodeIteratorResult result, UInt32IN mask)
{
   result.ptr = (AtNodeIterator*)AiUniverseGetNodeIterator(mask);
}

FABRIC_EXT_EXPORT void Arnold_UniverseGetNodeEntryIterator(AtNodeEntryIteratorResult result, UInt32IN mask)
{
   result.ptr = (AtNodeEntryIterator*)AiUniverseGetNodeEntryIterator(mask);
}

FABRIC_EXT_EXPORT void Arnold_UniverseGetAOVIterator(AtAOVIteratorResult result)
{
   result.ptr = (AtAOVIterator*)AiUniverseGetAOVIterator();
}

FABRIC_EXT_EXPORT void Arnold_NodeIteratorDestroy(AtNodeIteratorIO iter)
{
   AiNodeIteratorDestroy((AtNodeIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeIteratorGetNext(AtNodeResult result, AtNodeIteratorIO iter)
{
   result.ptr = (AtNode*)AiNodeIteratorGetNext((AtNodeIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_NodeIteratorFinished(AtNodeIteratorIN iter)
{
   return AiNodeIteratorFinished((AtNodeIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryIteratorDestroy(AtNodeEntryIteratorIO iter)
{
   AiNodeEntryIteratorDestroy((AtNodeEntryIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeEntryIteratorGetNext(AtNodeEntryResult result, AtNodeEntryIteratorIO iter)
{
   result.ptr = (AtNodeEntry*)AiNodeEntryIteratorGetNext((AtNodeEntryIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_NodeEntryIteratorFinished(AtNodeEntryIteratorIN iter)
{
   return AiNodeEntryIteratorFinished((AtNodeEntryIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_AOVIteratorDestroy(AtAOVIteratorIO iter)
{
   AiAOVIteratorDestroy((AtAOVIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_AOVIteratorGetNext(AtAOVEntryResult result, AtAOVIteratorIO iter)
{
   result.ptr = (AtAOVEntry*)AiAOVIteratorGetNext((AtAOVIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean Arnold_AOVIteratorFinished(AtAOVIteratorIN iter)
{
   return AiAOVIteratorFinished((AtAOVIterator*)iter.ptr);
}