#include "Arnold.h"

#include <ai.h>
#include <iostream>

#include "Arnold_types.h"

using namespace Fabric::EDK;

FABRIC_EXT_EXPORT void Arnold_Node(AtNodeResult result, StringIN name)
{
   result.ptr = AiNode(name.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeLookUpByName(AtNodeResult result, StringIN name)
{
   result.ptr = AiNodeLookUpByName(name.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeReset(AtNodeIO node)
{
   AiNodeReset((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeResetParameter(AtNodeIO node, StringIN param)
{
   AiNodeResetParameter((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeClone(AtNodeResult result, AtNodeIN node)
{
   result.ptr = AiNodeClone((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeDestroy(AtNodeIO node)
{
   return AiNodeDestroy((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeIs(AtNodeIN node, StringIN name)
{
   return AiNodeIs((AtNode*)node.ptr, name.data());
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeDeclare(AtNodeIO node, StringIN param, StringIN declaration)
{
   return AiNodeDeclare((AtNode*)node.ptr, param.data(), declaration.data());
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeLink(AtNodeIO node, StringIN param, AtNodeIN target)
{
   return AiNodeLink((AtNode*)node.ptr, param.data(), (AtNode*)target.ptr);
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeLinkOutput(AtNodeIN node, StringIN output, AtNodeIO target, StringIN input)
{
   return AiNodeLinkOutput((AtNode*)node.ptr, output.data(), (AtNode*)target.ptr, input.data());
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeUnlink(AtNodeIO node, StringIN input)
{
   return AiNodeUnlink((AtNode*)node.ptr, input.data());
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeIsLinked(AtNodeIN node, StringIN input)
{
   return AiNodeIsLinked((AtNode*)node.ptr, input.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeGetLink(AtNodeResult result, AtNodeIN node, StringIN input)
{
   result.ptr = AiNodeGetLink((AtNode*)node.ptr, input.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeGetLinkComp(AtNodeResult result, 
                                                AtNodeIN node,
                                                StringIN input,
                                                SInt32IN comp)
{
   int c = comp;
   result.ptr = AiNodeGetLink((AtNode*)node.ptr, input.data(), &c);
}

FABRIC_EXT_EXPORT void Arnold_NodeGetName(StringResult result, AtNodeIN node)
{
   result = AiNodeGetName((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeGetNodeEntry(AtNodeEntryResult result, AtNodeIN node)
{
   result.ptr = (AtNodeEntry*)AiNodeGetNodeEntry((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeGetParams(AtParamValueResult result, AtNodeIN node)
{
   result.ptr = (AtParamValue*)AiNodeGetParams((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetLocalData(AtNodeIO node, DataIN data)
{
   AiNodeSetLocalData((AtNode*)node.ptr, data);
}

FABRIC_EXT_EXPORT KL::Data Arnold_NodeGetLocalData(AtNodeIN node)
{
   return AiNodeGetLocalData((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeLookUpUserParameter(AtUserParamEntryResult result, AtNodeIN node, StringIN param)
{
   result.ptr = (AtUserParamEntry*)AiNodeLookUpUserParameter((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeGetUserParamIterator(AtUserParamIteratorResult result, AtNodeIN node)
{
   result.ptr = (AtUserParamIterator*)AiNodeGetUserParamIterator((AtNode*)node.ptr);
}

FABRIC_EXT_EXPORT void Arnold_UserParamIteratorDestroy(AtUserParamIteratorIO iter)
{
   AiUserParamIteratorDestroy((AtUserParamIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_UserParamIteratorGetNext(AtUserParamEntryResult result, AtUserParamIteratorIO iter)
{
   result.ptr = 
      (AtUserParamEntry*)AiUserParamIteratorGetNext((AtUserParamIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_UserParamIteratorFinished(AtUserParamIteratorIN iter)
{
   return AiUserParamIteratorFinished((AtUserParamIterator*)iter.ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetByte(AtNodeIO node, StringIN param, UInt8IN val)
{
   AiNodeSetByte((AtNode*)node.ptr, param.data(), val);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetInt(AtNodeIO node, StringIN param, SInt32IN val)
{
   AiNodeSetInt((AtNode*)node.ptr, param.data(), val);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetUInt(AtNodeIO node, StringIN param, UInt32IN val)
{
   AiNodeSetUInt((AtNode*)node.ptr, param.data(), val);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetBool(AtNodeIO node, StringIN param, BooleanIN val)
{
   AiNodeSetBool((AtNode*)node.ptr, param.data(), val);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetFlt(AtNodeIO node, StringIN param, Float32IN val)
{
   AiNodeSetFlt((AtNode*)node.ptr, param.data(), val);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetRGB(AtNodeIO node, StringIN param, Float32IN r, Float32IN g, Float32IN b)
{
   AiNodeSetRGB((AtNode*)node.ptr, param.data(), r, g, b);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetRGBA(AtNodeIO node, StringIN param, Float32IN r, Float32IN g, Float32IN b, 
                                             Float32IN a)
{
   AiNodeSetRGBA((AtNode*)node.ptr, param.data(), r, g, b, a);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetVec(AtNodeIO node, StringIN param, Float32IN x, Float32IN y, Float32IN z)
{
   AiNodeSetVec((AtNode*)node.ptr, param.data(), x, y, z);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetPnt(AtNodeIO node, StringIN param, Float32IN x, Float32IN y, Float32IN z)
{
   AiNodeSetPnt((AtNode*)node.ptr, param.data(), x, y, z);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetPnt2(AtNodeIO node, StringIN param, Float32IN x, Float32IN y)
{
   AiNodeSetPnt2((AtNode*)node.ptr, param.data(), x, y);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetStr(AtNodeIO node, StringIN param, StringIN str)
{
   AiNodeSetStr((AtNode*)node.ptr, param.data(), str.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeSetPtr(AtNodeIO node, StringIN param, DataIN ptr)
{
   AiNodeSetPtr((AtNode*)node.ptr, param.data(), ptr);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetArray(AtNodeIO node, StringIN param, AtArrayIO array)
{
   AtArray* arr = (AtArray*)array.ptr;
   // std::cerr << "Setting array '" << param.data() << "' with " << arr->nelements << " elements" << "\n";
   AiNodeSetArray((AtNode*)node.ptr, param.data(), arr);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetMatrix(AtNodeIO node, StringIN param, Mat44IN matrix)
{
   AtMatrix mtx = {
      {matrix.row0.x, matrix.row0.y, matrix.row0.z, matrix.row0.t},
      {matrix.row1.x, matrix.row1.y, matrix.row1.z, matrix.row1.t},
      {matrix.row2.x, matrix.row2.y, matrix.row2.z, matrix.row2.t},
      {matrix.row3.x, matrix.row3.y, matrix.row3.z, matrix.row3.t},
   };
   AiNodeSetMatrix((AtNode*)node.ptr, param.data(), mtx);
}

FABRIC_EXT_EXPORT void Arnold_NodeSetAttributes(AtNodeIO node, StringIN attributes)
{
   AiNodeSetAttributes((AtNode*)node.ptr, attributes.data());
}

FABRIC_EXT_EXPORT KL::UInt8 Arnold_NodeGetByte(AtNodeIN node, StringIN param)
{
   return AiNodeGetByte((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_NodeGetBool(AtNodeIN node, StringIN param)
{
   return AiNodeGetBool((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT KL::SInt32 Arnold_NodeGetInt(AtNodeIN node, StringIN param)
{
   return AiNodeGetInt((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT KL::UInt32 Arnold_NodeGetUInt(AtNodeIN node, StringIN param)
{
   return AiNodeGetUInt((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT KL::Float32 Arnold_NodeGetFlt(AtNodeIN node, StringIN param)
{
   return AiNodeGetFlt((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeGetRGB(ColorResult result, AtNodeIN node, StringIN param)
{
   AtRGB rgb = AiNodeGetRGB((AtNode*)node.ptr, param.data());
   result.r = rgb.r;
   result.g = rgb.g;
   result.b = rgb.b;
   result.a = 1.0f; //< KL convention is that a Color that does not have alpha explicitly constructed has alpha = 1
}

FABRIC_EXT_EXPORT void Arnold_NodeGetRGBA(ColorResult result, AtNodeIN node, StringIN param)
{
   AtRGBA rgba = AiNodeGetRGBA((AtNode*)node.ptr, param.data());
   result.r = rgba.r;
   result.g = rgba.g;
   result.b = rgba.b;
   result.a = rgba.a;
}

FABRIC_EXT_EXPORT void Arnold_NodeGetVec(Vec3Result result, AtNodeIN node, StringIN param)
{
   AtVector vec = AiNodeGetVec((AtNode*)node.ptr, param.data());
   result.x = vec.x;
   result.y = vec.y;
   result.z = vec.z;
}

FABRIC_EXT_EXPORT void Arnold_NodeGetPnt(Vec3Result result, AtNodeIN node, StringIN param)
{
   AtPoint pnt = AiNodeGetPnt((AtNode*)node.ptr, param.data());
   result.x = pnt.x;
   result.y = pnt.y;
   result.z = pnt.z;
}

FABRIC_EXT_EXPORT void Arnold_NodeGetPnt2(Vec2Result result, AtNodeIN node, StringIN param)
{
   AtPoint2 pnt = AiNodeGetPnt2((AtNode*)node.ptr, param.data());
   result.x = pnt.x;
   result.y = pnt.y;
}

FABRIC_EXT_EXPORT void Arnold_NodeGetStr(StringResult result, AtNodeIN node, StringIN param)
{
   result = AiNodeGetStr((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT KL::Data Arnold_NodeGetPtr(AtNodeIN node, StringIN param)
{
   return AiNodeGetPtr((AtNode*)node.ptr, param.data());
}

FABRIC_EXT_EXPORT void Arnold_NodeGetArray(AtArrayResult result, AtNodeIN node, StringIN param)
{
   AtArray* arr = AiNodeGetArray((AtNode*)node.ptr, param.data());
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_NodeGetMatrix(Mat44Result result, AtNodeIN node, StringIN param)
{
   AtMatrix mtx;
   AiNodeGetMatrix((AtNode*)node.ptr, param.data(), mtx);
   memcpy(&result.row0, mtx[0], sizeof(float)*4);
   memcpy(&result.row1, mtx[1], sizeof(float)*4);
   memcpy(&result.row2, mtx[2], sizeof(float)*4);
   memcpy(&result.row3, mtx[3], sizeof(float)*4);
}

