#include "./Arnold.h"

#include <ai_render.h>

using namespace Fabric::EDK;

FABRIC_EXT_EXPORT void Arnold_Begin()
{
   AiBegin();
}

FABRIC_EXT_EXPORT void Arnold_End()
{
   AiEnd();
}

FABRIC_EXT_EXPORT KL::Integer Arnold_Render(KL::Traits<KL::SInt32>::INParam mode)
{
   return AiRender(mode);
}

FABRIC_EXT_EXPORT void Arnold_RenderAbort()
{
   AiRenderAbort();
}

FABRIC_EXT_EXPORT void Arnold_RenderInterrupt()
{
   AiRenderInterrupt();
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_Rendering()
{
   return AiRendering();
}