const Integer AI_SEVERITY_INFO    = 0x00;  
const Integer AI_SEVERITY_WARNING = 0x01;  
const Integer AI_SEVERITY_ERROR   = 0x02; 
const Integer AI_SEVERITY_FATAL   = 0x03;  

const Integer AI_LOG_NONE         = 0x0000;  /**< don't show any messages                                  */
const Integer AI_LOG_INFO         = 0x0001;  /**< show all regular information messages                    */
const Integer AI_LOG_WARNINGS     = 0x0002;  /**< show warning messages                                    */
const Integer AI_LOG_ERRORS       = 0x0004;  /**< show error messages                                      */
const Integer AI_LOG_DEBUG        = 0x0008;  /**< show debug messages                                      */
const Integer AI_LOG_STATS        = 0x0010;  /**< show detailed render statistics                          */
const Integer AI_LOG_ASS_PARSE    = 0x0020;  /**< show .ass-file parsing details                           */
const Integer AI_LOG_PLUGINS      = 0x0040;  /**< show details about plugins loaded                        */
const Integer AI_LOG_PROGRESS     = 0x0080;  /**< show progress messages at 5% increments while rendering  */
const Integer AI_LOG_NAN          = 0x0100;  /**< show warnings for pixels with NaN's                      */
const Integer AI_LOG_TIMESTAMP    = 0x0200;  /**< prefix messages with a timestamp (elapsed time)          */
const Integer AI_LOG_BACKTRACE    = 0x0400;  /**< show stack contents after abnormal program termination (\c SIGSEGV, etc) */
const Integer AI_LOG_MEMORY       = 0x0800;  /**< prefix messages with current memory usage                */
const Integer AI_LOG_COLOR        = 0x1000;  /**< add colors to log messages based on severity             */
const Integer AI_LOG_SSS          = 0x2000;  /**< show messages about sub-surface scattering pointclouds   */

/** set all flags at once */
const Integer AI_LOG_ALL = 
 ( AI_LOG_INFO       | AI_LOG_WARNINGS   | AI_LOG_ERRORS    |
   AI_LOG_DEBUG      | AI_LOG_STATS      | AI_LOG_PLUGINS   |
   AI_LOG_PROGRESS   | AI_LOG_NAN        | AI_LOG_ASS_PARSE |
   AI_LOG_TIMESTAMP  | AI_LOG_BACKTRACE  | AI_LOG_MEMORY    |
   AI_LOG_COLOR      | AI_LOG_SSS);


function AiMsgSetLogFileName(String filename) = "Arnold_MsgSetLogFileName";
function AiMsgSetLogFileFlags(SInt32 flags) = "Arnold_MsgSetLogFileFlags";
function AiMsgSetConsoleFlags(SInt32 flags) = "Arnold_MsgSetConsoleFlags";
function AiMsgSetMaxWarnings(SInt32 max_warnings) = "Arnold_MsgSetMaxWarnings";

function UInt64 AiMsgUtilGetUsedMemory() = "Arnold_MsgUtilGetUsedMemory";
function UInt32 AiMsgUtilGetElapsedTime() = "Arnold_MsgUtilGetElapsedTime";
